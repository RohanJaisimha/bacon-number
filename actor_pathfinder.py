from collections import deque
from constants import ACTORS_COSTARS_FILE_DELIMITER
from constants import ACTORS_NAMES_IDENTIFIER_MAP_FILENAME_JSON
from constants import KEVIN_BACON_IDENTIFIER
from constants import MOVIES_NAMES_IDENTIFIER_MAP_FILENAME_JSON
from datetime import datetime
from Logger import Logger
from utilities import get_full_path_for_costars_file
from utilities import load_json

import random
import sys

logger = Logger(sys.stdout)
ACTORS_NAMES_IDENTIFIER_MAP = load_json(ACTORS_NAMES_IDENTIFIER_MAP_FILENAME_JSON)
MOVIES_NAMES_IDENTIFIER_MAP = load_json(MOVIES_NAMES_IDENTIFIER_MAP_FILENAME_JSON)


def get_common_movie_identifier(actor1_identifier, actor2_identifier):
    common_movie_identifiers = []
    fin = open(get_full_path_for_costars_file(actor1_identifier), "r")

    for line in fin:
        costar_identifier, movie_identifier = line.strip().split(
            ACTORS_COSTARS_FILE_DELIMITER
        )

        if costar_identifier == actor2_identifier:
            common_movie_identifiers.append(movie_identifier)

    fin.close()

    return random.choice(common_movie_identifiers)


def decrypt_actor_path(path):
    decrypted_path = []

    for i in range(len(path) - 1):
        actor1_identifier, actor2_identifier = path[i], path[i + 1]
        common_movie_identifier = get_common_movie_identifier(
            actor1_identifier, actor2_identifier
        )
        actor1_name = ACTORS_NAMES_IDENTIFIER_MAP[actor1_identifier]
        actor2_name = ACTORS_NAMES_IDENTIFIER_MAP[actor2_identifier]
        common_movie_name = MOVIES_NAMES_IDENTIFIER_MAP[common_movie_identifier]
        decrypted_path.append([actor1_name, common_movie_name, actor2_name])

    return decrypted_path


def create_actor_path(actors_visited, actor_identifier):
    path = []

    while actor_identifier != None:
        path.append(actor_identifier)
        actor_identifier = actors_visited[actor_identifier]

    return path


# Finds the path between the actors using BFS
def find_actor_path_helper(origin, destination, to_log):
    q = deque()

    q.append((0, origin, None))

    num_layers_deep = 0

    actors_visited = {}

    while len(q) > 0:
        distance, actor_identifier, costar_identifier = q.popleft()

        if actor_identifier in actors_visited:
            continue

        if num_layers_deep < distance:
            num_layers_deep = distance
            if to_log:
                logger.log(
                    f"{num_layers_deep} layer(s) deep, there are {len(q)} actors at this level"
                )

        actors_visited[actor_identifier] = costar_identifier

        if actor_identifier == destination:
            return create_actor_path(actors_visited, actor_identifier)

        fin = open(get_full_path_for_costars_file(actor_identifier), "r")
        for line in fin:
            costar_identifier, movie_identifier = line.strip().split(
                ACTORS_COSTARS_FILE_DELIMITER
            )
            q.append((distance + 1, costar_identifier, actor_identifier))
        fin.close()

    return None


def find_actor_path(origin, destination, to_log=True):
    if to_log:
        logger.log(f"Finding path from {origin} to {destination}")

    path = find_actor_path_helper(origin, destination, to_log)

    if path != None:
        return decrypt_actor_path(path)
    else:
        return None


def main():
    origin = input("Enter the identifier: ").strip()
    destination = input("Enter the identifier (leave blank for Kevin Bacon): ").strip()
    destination = KEVIN_BACON_IDENTIFIER if not destination else destination

    path = find_path(origin, destination)

    print()
    for actor1_name, common_movie_name, actor2_name in path:
        print(f"{actor1_name} worked on {common_movie_name} with {actor2_name}")


if __name__ == "__main__":
    main()
