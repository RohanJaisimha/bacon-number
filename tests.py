from constants import ACTOR_COSTARS_FILENAME_1
from constants import ACTOR_COSTARS_FILENAME_2
from constants import ACTOR_COSTARS_FILENAME_3
from actor_pathfinder import create_actor_path
from actor_pathfinder import decrypt_actor_path
from actor_pathfinder import get_common_movie_identifier
from utilities import get_full_path_for_costars_file

import unittest


class TestEverything(unittest.TestCase):
    # Testing the case where the file is in the first directory
    def test_get_full_path_for_costars_file_1(self):
        MACAULY_CULKIN_IDENTIFIER = "nm0000346"
        self.assertEqual(
            get_full_path_for_costars_file(MACAULY_CULKIN_IDENTIFIER),
            ACTOR_COSTARS_FILENAME_1.format(MACAULY_CULKIN_IDENTIFIER),
        )

    # Testing the case where the file is in the second directory
    def test_get_full_path_for_costars_file_2(self):
        ZAC_EFRON_IDENTIFIER = "nm1374980"
        self.assertEqual(
            get_full_path_for_costars_file(ZAC_EFRON_IDENTIFIER),
            ACTOR_COSTARS_FILENAME_2.format(ZAC_EFRON_IDENTIFIER),
        )

    # Testing the case where the file is in the third directory
    def test_get_full_path_for_costars_file_3(self):
        JACOB_TREMBLAY_IDENTIFIER = "nm5016878"
        self.assertEqual(
            get_full_path_for_costars_file(JACOB_TREMBLAY_IDENTIFIER),
            ACTOR_COSTARS_FILENAME_3.format(JACOB_TREMBLAY_IDENTIFIER),
        )

    # Testing the case where the file does not exist
    def test_get_full_path_for_costars_file_4(self):
        RANDOM_NON_ACTOR_IDENTIFIER = "nm4630347"
        self.assertIsNone(get_full_path_for_costars_file(RANDOM_NON_ACTOR_IDENTIFIER))

    # Testing a case where there is a common movie between two actors
    def test_get_common_movie_identifier_1(self):
        ZAC_EFRON_IDENTIFIER = "nm1374980"
        VANESSA_HUDGENS_IDENTIFIER = "nm1227814"

        get_common_movie_identifier(ZAC_EFRON_IDENTIFIER, VANESSA_HUDGENS_IDENTIFIER)

    # Testing the case where there is NOT a common movie between two actors
    def test_get_common_movie_identifier_2(self):
        ZAC_EFRON_IDENTIFIER = "nm0891786"
        AAMIR_KHAN_IDENTIFIER = "nm0451148"

        self.assertRaises(
            IndexError,
            get_common_movie_identifier,
            ZAC_EFRON_IDENTIFIER,
            AAMIR_KHAN_IDENTIFIER,
        )

    # Not really a test, more of a check that it does throw any errors
    def test_decrypt_actor_path(self):
        KEVIN_BACON_IDENTIFIER = "nm0000102"
        KRISTIN_CHARNEY_IDENTIFIER = "nm2190740"
        ERIC_JEWETT_IDENTIFIER = "nm0422464"
        PAUL_STADER_IDENTIFIER = "nm0821126"

        SAMPLE_PATH = [
            KEVIN_BACON_IDENTIFIER,
            KRISTIN_CHARNEY_IDENTIFIER,
            ERIC_JEWETT_IDENTIFIER,
            PAUL_STADER_IDENTIFIER,
        ]

        decrypt_actor_path(SAMPLE_PATH)

    def test_create_actor_path(self):
        sample = {
            "nm0000102": None,
            "nm2190740": "nm0000102",
            "nm0422464": "nm2190740",
            "nm0821126": "nm0422464",
        }

        self.assertEqual(
            ["nm0821126", "nm0422464", "nm2190740", "nm0000102"],
            create_actor_path(sample, "nm0821126"),
        )


if __name__ == "__main__":
    unittest.main()
