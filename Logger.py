from datetime import datetime


class Logger:
    def __init__(self, logger):
        self.logger = logger

    def log(self, text):
        timestamp = str(datetime.now())
        self.logger.write(timestamp + " " + str(text) + "\n")
        self.logger.flush()
