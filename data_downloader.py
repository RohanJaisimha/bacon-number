from constants import ACTORS_RAW_DATA_TSV_FILENAME
from constants import ACTORS_RAW_DATA_ZIPFILE_URL
from constants import MOVIES_RAW_DATA_TSV_FILENAME
from constants import MOVIES_RAW_DATA_ZIPFILE_URL
from datetime import datetime
from gzip import GzipFile
from io import BytesIO
from Logger import Logger
from urllib.request import urlopen

import sys

logger = Logger(sys.stdout)


def download_and_unzip(url, filename, to_log=True):
    if to_log:
        logger.log(f"Downloading from: {url}")
    response = urlopen(url)
    compressed_file = BytesIO(response.read())
    decompressed_file = GzipFile(fileobj=compressed_file)

    fout = open(filename, "wb")
    fout.write(decompressed_file.read())
    fout.close()


def main():
    urls_filenames_map = {
        ACTORS_RAW_DATA_ZIPFILE_URL: ACTORS_RAW_DATA_TSV_FILENAME,
        MOVIES_RAW_DATA_ZIPFILE_URL: MOVIES_RAW_DATA_TSV_FILENAME,
    }

    for url, filename in urls_filenames_map.items():
        download_and_unzip(url, filename)


if __name__ == "__main__":
    main()
