from constants import ACTOR_COSTARS_FILENAME_1
from constants import KEVIN_BACON_IDENTIFIER
from flask import Flask
from flask import jsonify
from flask import render_template
from flask import request

import os
import sys


if not os.path.exists(ACTOR_COSTARS_FILENAME_1.format(KEVIN_BACON_IDENTIFIER)):
    print("Please run `make setup` and try again")
    sys.exit(0)

application = Flask(__name__)


@application.route("/")
def index():
    return render_template("home.html", links={"Actor": "actor", "Movie": "movie"})


@application.route("/actor")
def actor():
    return render_template("actor.html")


@application.route("/movie")
def movie():
    return render_template("movie.html")


@application.route("/find_actor_path", methods=["POST"])
def find_actor_path():
    import actor_pathfinder

    origin = request.form["origin"]
    destination = request.form["destination"]

    return jsonify(actor_pathfinder.find_actor_path(origin, destination))


@application.route("/find_movie_path", methods=["POST"])
def find_movie_path():
    import movie_pathfinder

    movie_1 = request.form["movie_1"]
    movie_2 = request.form["movie_2"]

    return jsonify(movie_pathfinder.find_movie_path(movie_1, movie_2))


if __name__ == "__main__":
    application.run(debug=True, host="0.0.0.0")
