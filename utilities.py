from constants import ACTOR_COSTARS_FILENAME_1
from constants import ACTOR_COSTARS_FILENAME_2
from constants import ACTOR_COSTARS_FILENAME_3

import json
import os


def save_json(data, filename):
    fout = open(filename, "w")
    json.dump(data, fout, indent=4, sort_keys=True)
    fout.close()


def load_json(filename):
    fin = open(filename, "r")
    data = json.load(fin)
    fin.close()

    return data


def get_full_path_for_costars_file(actor_identifier):
    if os.path.exists(ACTOR_COSTARS_FILENAME_1.format(actor_identifier)):
        return ACTOR_COSTARS_FILENAME_1.format(actor_identifier)
    elif os.path.exists(ACTOR_COSTARS_FILENAME_2.format(actor_identifier)):
        return ACTOR_COSTARS_FILENAME_2.format(actor_identifier)
    elif os.path.exists(ACTOR_COSTARS_FILENAME_3.format(actor_identifier)):
        return ACTOR_COSTARS_FILENAME_3.format(actor_identifier)
    else:
        return None
