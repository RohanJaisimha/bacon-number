SHELL := /bin/bash

test:
	python3 tests.py

tests:
	python3 tests.py

format:
	black *.py

coverage:
	coverage run tests.py
	coverage report -m
	rm -f .coverage

setup:
	pip install -r requirements.txt && python3 data_downloader.py && python3 data_processor.py && python3 graph_maker.py
