from constants import ACCEPTABLE_TITLE_TYPES
from constants import ACTORS_NAMES_IDENTIFIER_MAP_FILENAME_JSON
from constants import ACTORS_RAW_DATA_TSV_FILENAME
from constants import MOVIES_CAST_MAP_FILENAME_JSON
from constants import MOVIES_NAMES_IDENTIFIER_MAP_FILENAME_JSON
from constants import MOVIES_RAW_DATA_TSV_FILENAME
from datetime import datetime
from Logger import Logger
from unidecode import unidecode
from utilities import save_json

import sys

ACTORS_NAMES_IDENTIFIER_MAP = {}
MOVIES_NAMES_IDENTIFIER_MAP = {}
MOVIES_CAST_MAP = {}

logger = Logger(sys.stdout)


def read_actors_tsv():
    global ACTORS_NAMES_IDENTIFIER_MAP, MOVIES_NAMES_IDENTIFIER_MAP, MOVIES_CAST_MAP

    fin = open(ACTORS_RAW_DATA_TSV_FILENAME, "r", encoding="utf-8")

    for line in fin:
        (
            nconst,
            primary_name,
            birth_year,
            death_year,
            primary_profession,
            known_for_titles,
        ) = unidecode(line.strip()).split("\t")
        known_for_titles = known_for_titles.split(",")
        if (
            "actor" not in primary_profession
            and "actress" not in primary_profession
            or (len(known_for_titles) == 1 and known_for_titles[0] == "\\N")
        ):
            continue
        for known_for_title in known_for_titles:
            if known_for_title in MOVIES_NAMES_IDENTIFIER_MAP:
                ACTORS_NAMES_IDENTIFIER_MAP[nconst] = primary_name

                if known_for_title not in MOVIES_CAST_MAP:
                    MOVIES_CAST_MAP[known_for_title] = []

                if nconst not in MOVIES_CAST_MAP[known_for_title]:
                    MOVIES_CAST_MAP[known_for_title].append(nconst)

    fin.close()


def read_movies_tsv():
    global MOVIES_NAMES_IDENTIFIER_MAP

    fin = open(MOVIES_RAW_DATA_TSV_FILENAME, "r", encoding="utf-8")

    for line in fin:
        (
            tconst,
            title_type,
            primary_title,
            original_title,
            is_adult,
            start_year,
            end_year,
            runtime_minutes,
            genres,
        ) = unidecode(line.strip()).split("\t")
        is_adult = bool(is_adult == "1")
        if title_type in ACCEPTABLE_TITLE_TYPES and not is_adult:
            MOVIES_NAMES_IDENTIFIER_MAP[tconst] = (
                primary_title + " (" + start_year + ")"
            )

    fin.close()


def clean_MOVIES_NAMES_IDENTIFIER_MAP():
    global MOVIES_NAMES_IDENTIFIER_MAP, MOVIES_CAST_MAP

    movie_identifiers_to_remove = set(MOVIES_NAMES_IDENTIFIER_MAP.keys()) - set(
        MOVIES_CAST_MAP.keys()
    )

    for movie_identifier in movie_identifiers_to_remove:
        del MOVIES_NAMES_IDENTIFIER_MAP[movie_identifier]


def main():
    global ACTORS_NAMES_IDENTIFIER_MAP, MOVIES_NAMES_IDENTIFIER_MAP, MOVIES_CAST_MAP

    logger.log("Starting processing of data")
    read_movies_tsv()
    logger.log("Finished processing movies")
    read_actors_tsv()
    logger.log("Finished processing actors")
    clean_MOVIES_NAMES_IDENTIFIER_MAP()
    logger.log("Finished cleaning MOVIES_NAMES_IDENTIFIER_MAP")
    save_json(MOVIES_NAMES_IDENTIFIER_MAP, MOVIES_NAMES_IDENTIFIER_MAP_FILENAME_JSON)
    save_json(ACTORS_NAMES_IDENTIFIER_MAP, ACTORS_NAMES_IDENTIFIER_MAP_FILENAME_JSON)
    save_json(MOVIES_CAST_MAP, MOVIES_CAST_MAP_FILENAME_JSON)
    logger.log("Finished processing")


if __name__ == "__main__":
    main()
