from constants import MOVIES_CAST_MAP_FILENAME_JSON
from Logger import Logger
from pathfinder import find_actor_path
from utilities import load_json

import sys

MOVIES_CAST_MAP = load_json(MOVIES_CAST_MAP_FILENAME_JSON)
logger = Logger(sys.stdout)


def find_movie_path(movie_1, movie_2):
    shortest_path = None
    total_pairs = len(MOVIES_CAST_MAP[movie_1]) * len(MOVIES_CAST_MAP[movie_2])
    i = 0
    logger.log(f"There are {total_pairs} pairs of actors")
    for movie_1_actor_identifier in MOVIES_CAST_MAP[movie_1]:
        for movie_2_actor_identifier in MOVIES_CAST_MAP[movie_2]:
            if total_pairs < 20 or i % (total_pairs // 20) == 0:
                percentage_processed = i // (total_pairs // 20) * 5
                logger.log(f"Gone through {i} pairs ({percentage_processed}%)")
            path = find_actor_path(
                movie_1_actor_identifier, movie_2_actor_identifier, to_log=False
            )
            if not shortest_path or len(path) < len(shortest_path):
                shortest_path = path

            i += 1

    return shortest_path


def main():
    movie_1 = input("Enter the identifier: ").strip()
    movie_2 = input("Enter the second identifier: ").strip()

    path = find_movie_path(movie_1, movie_2)

    print(f"\n\nThe shortest path is {path} - size {len(path)}")


if __name__ == "__main__":
    main()
