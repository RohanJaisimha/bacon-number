# Bacon Number

Bacon number calculator

# How to Run
1. First, have python >= 3.6, pip, and git
2. `git clone https://gitlab.com/RohanJaisimha/bacon-number.git`
3. `cd bacon-number`
4. `make setup` - Should take 2-3 hours or so. If this succeeds, skip to step 8. Else, proceed to step 9 and continue.
5. `pip3 install -r requirements.txt`
6. `python3 data_downloader.py` - Should take 3 minutes at most
7. `python3 data_processor.py` - Should take 2 minutes at most
8. `python3 graph_maker.py` - Should be done in 1-2 hours, let it run overnight to be sure
9. `python3 pathfinder.py` to run the calculator
10. To run unit tests, `python3 tests.py` - make sure you only run this after step 8, otherwise they will fail :(

# Files
* constants.py - Contains all the constants
* data_processor.py - Processes the raw tsvs from IMDB and prepares three JSON files
* data_downloader.py - Downloads and unzips the files from IMDB
* pathfinder.py - The actual calculator file that uses [Breadth-first Search](https://en.wikipedia.org/wiki/Breadth-first_search) to find the Bacon number of someone
* graph_maker.py - Processes the JSON files and to create ~2 million text files - each for one actor
* utilities.py - Contains all the utility functions that are shared amongest the main three files
* tests.py - Contains all the unit tests
* Data/Maps/ACTORS_NAMES_IDENTIFIER_MAP.json - A dictionary that contains the mapping of IMDB identifiers for actors to their name
* Data/Maps/MOVIESS_NAMES_IDENTIFIER_MAP.json - A dictionary that contains the mapping of IMDB identifiers for movies to its name
* Data/Maps/MOVIES_CAST_MAP.json - A dictionary that contains the mapping of IMDB identifiers of movies to a list of all of the actors in the movie
* Data/Processed_Data_[1/2/3]/COSTARS_OF_[ACTOR_IDENTIFIER].txt - A text file where each line contains the costars of [ACTOR_IDENTIFIER] and the movie that the two actors were in. There are about 2 million of these text files, split roughly equally between three directories
