ACTOR_COSTARS_FILENAME_1 = "./Data/Costars_1/COSTARS_OF_{}.txt"
ACTOR_COSTARS_FILENAME_2 = "./Data/Costars_2/COSTARS_OF_{}.txt"
ACTOR_COSTARS_FILENAME_3 = "./Data/Costars_3/COSTARS_OF_{}.txt"

ACTORS_NAMES_IDENTIFIER_MAP_FILENAME_JSON = (
    "./Data/Maps/ACTORS_NAMES_IDENTIFIER_MAP.json"
)

MOVIES_NAMES_IDENTIFIER_MAP_FILENAME_JSON = (
    "./Data/Maps/MOVIES_NAMES_IDENTIFIER_MAP.json"
)

MOVIES_CAST_MAP_FILENAME_JSON = "./Data/Maps/MOVIES_CAST_MAP.json"

KEVIN_BACON_IDENTIFIER = "nm0000102"

ACTORS_RAW_DATA_TSV_FILENAME = "./Data/Raw_Data/name.basics.tsv"

MOVIES_RAW_DATA_TSV_FILENAME = "./Data/Raw_Data/title.basics.tsv"

ACTORS_RAW_DATA_ZIPFILE_URL = "https://datasets.imdbws.com/name.basics.tsv.gz"

MOVIES_RAW_DATA_ZIPFILE_URL = "https://datasets.imdbws.com/title.basics.tsv.gz"

ACCEPTABLE_TITLE_TYPES = {"tvMovie", "movie"}

ACTORS_COSTARS_FILE_DELIMITER = ","
