from constants import ACTOR_COSTARS_FILENAME_1
from constants import ACTOR_COSTARS_FILENAME_2
from constants import ACTOR_COSTARS_FILENAME_3
from constants import ACTORS_COSTARS_FILE_DELIMITER
from constants import ACTORS_NAMES_IDENTIFIER_MAP_FILENAME_JSON
from constants import ACTORS_RAW_DATA_TSV_FILENAME
from constants import MOVIES_CAST_MAP_FILENAME_JSON
from datetime import datetime
from Logger import Logger
from unidecode import unidecode
from utilities import load_json

import sys

logger = Logger(sys.stdout)


def populate_graph(ACTORS_NAMES_IDENTIFIER_MAP, MOVIES_CAST_MAP, to_log=True):
    fin = open(ACTORS_RAW_DATA_TSV_FILENAME, "r", encoding="utf-8")

    i = 0
    already_printed = False

    num_actors = sum(1 for actor in ACTORS_NAMES_IDENTIFIER_MAP if actor[:2] == "nm")

    for line in fin:
        if i % (num_actors // 20) == 0 and not already_printed:
            percentage_processed = i // (num_actors // 20) * 5
            if to_log:
                logger.log(f"Processed {i} actors ({percentage_processed}%)")
            already_printed = True
        (
            nconst,
            primary_name,
            birth_year,
            death_year,
            primary_profession,
            known_for_titles,
        ) = unidecode(line.strip()).split("\t")
        known_for_titles = known_for_titles.split(",")
        if nconst not in ACTORS_NAMES_IDENTIFIER_MAP:
            continue
        i += 1
        already_printed = False
        del ACTORS_NAMES_IDENTIFIER_MAP[nconst]
        output = ""
        for known_for_title in known_for_titles:
            if known_for_title in MOVIES_CAST_MAP:
                for costar_identifier in MOVIES_CAST_MAP[known_for_title]:
                    if nconst == costar_identifier:
                        continue
                    output += (
                        costar_identifier
                        + ACTORS_COSTARS_FILE_DELIMITER
                        + known_for_title
                        + "\n"
                    )
        if output:
            if i < num_actors // 3:
                fout = open(ACTOR_COSTARS_FILENAME_1.format(nconst), "wt")
            elif i < 2 * num_actors // 3:
                fout = open(ACTOR_COSTARS_FILENAME_2.format(nconst), "wt")
            else:
                fout = open(ACTOR_COSTARS_FILENAME_3.format(nconst), "wt")
            fout.write(output)
            fout.flush()
            fout.close()


def main():
    logger.log("Starting graph_maker")
    ACTORS_NAMES_IDENTIFIER_MAP = load_json(ACTORS_NAMES_IDENTIFIER_MAP_FILENAME_JSON)
    MOVIES_CAST_MAP = load_json(MOVIES_CAST_MAP_FILENAME_JSON)
    logger.log("Read all stored data")
    populate_graph(ACTORS_NAMES_IDENTIFIER_MAP, MOVIES_CAST_MAP)


if __name__ == "__main__":
    main()
